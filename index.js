// 

//od - I am not sure why you have the synthax plunkerApp.factoryu; this is wrong; it should be my App (or whatever name).facotry
var myApp = angular
			.module('plunkerApp', ['ngMaterial', 'ngAnimate']) //there was an extra bracket



			.factory('FilterData', function() {
			  var filterSpecs = {
			    topChoices : [{
			            id: "cool",
			            name: "cool",
			            selected: false     
			        },{
			            id: "sCool",
			            name: "Super Cool",
			            selected: false
			        },{
			            id: "eCool",
			            name: "Easy Cool",
			            selected: false
			        }
			    ],
			    price: {
			      low: 25,
			      high: 200
			    }
			  };
			  return filterSpecs;
			}) // do not put ; after brackets; surprisingly Angular can't process the data after it

			.factory('ProductData', function(){
				  var products = [{
				    name: "Cool Thing 1",
				    desc: "bla bla bla",
				    onPromotion : true,
				    sellers: [{
				      sellerName : "Bob's",
				      price: 10
				      },{
				      sellerName : "Tankies",
				      price: 7
				      },{
				      sellerName : "CoolSite",
				      price: 12
				      }
				    ]
				    },{
				    name: "Cool Thing 2",
				    desc: "bla bla bla2",
				    onPromotion : false,
				    sellers: [{
				      sellerName : "Bob's",
				      price: 12
				      },{
				      sellerName : "The Market",
				      price: 11
				      },{
				      sellerName : "CoolSite",
				      price: 9
				      }
				    ]
				    },{
				    name: "Cool Thing 3",
				    desc: "bla bla bla",
				    onPromotion : true,
				    sellers: [{
				      sellerName : "Bob's",
				      price: 11
				      },{
				      sellerName : "Tankies",
				      price: 12.5
				      },{
				      sellerName : "CoolSite",
				      price: 13
				      }
				    ]
				    }
				  ];
				  return products;
				})
			.controller('prodCtrl', [
			  '$scope', '$http', 'FilterData', 'ProductData', function(
			    $scope, $http, FilterData, ProductData){
			      $scope.filterData = FilterData;
			      $scope.products = ProductData;
			}]);